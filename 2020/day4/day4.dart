import 'dart:io';

main(List<String> args) {
  File input = File("input");
  List<Passport> passports = [];

  input.readAsString().then((value) {
    value.split("\n\n").forEach((passStr) {
      passStr = passStr.replaceAll("\n", " ").trim();
      var p = Passport.fromInput(passStr);
      passports.add(Passport.fromInput(passStr));
    });

    print(passports.where((element) => element.isValid()).length);
  });
}

class Passport {
  static const String BYR = "byr";
  static const String IYR = "iyr";
  static const String EYR = "eyr";
  static const String HGT = "hgt";
  static const String HCL = "hcl";
  static const String ECL = "ecl";
  static const String PID = "pid";
  static const String CID = "cid";

  String? byr;
  String? iyr;
  String? eyr;
  String? hgt;
  String? hcl;
  String? ecl;
  String? pid;
  String? cid;

  Passport();

  factory Passport.fromInput(String input) {
    Passport passport = Passport();
    List<String> datas = input.split(' ');
    datas.forEach((line) {
      String type = line.split(":")[0];
      String data = line.split(":")[1].trim();
      switch (type) {
        case BYR:
          passport.byr = data;
          break;
        case IYR:
          passport.iyr = data;
          break;
        case EYR:
          passport.eyr = data;
          break;
        case HGT:
          passport.hgt = data;
          break;
        case ECL:
          passport.ecl = data;
          break;
        case HCL:
          passport.hcl = data;
          break;
        case PID:
          passport.pid = data;
          break;
        case CID:
          passport.cid = data;
          break;
      }
    });

    return passport;
  }

  bool isValid() {
    return byr != null &&
        iyr != null &&
        eyr != null &&
        hgt != null &&
        ecl != null &&
        hcl != null &&
        pid != null;
  }

  String toString() {
    return "$BYR:$byr $IYR:$iyr $EYR:$eyr $HGT:$hgt $ECL:$ecl $HCL:$hcl $PID:$pid $CID:$cid \n VALID:${this.isValid()}";
  }
}
