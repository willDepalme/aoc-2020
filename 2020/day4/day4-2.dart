import 'dart:io';

main(List<String> args) {
  File input = File("input");
  List<Passport> passports = [];

  input.readAsString().then((value) {
    value.split("\n\n").forEach((passStr) {
      passStr = passStr.replaceAll("\n", " ").trim();
      //print(passStr);
      var p = Passport.fromInput(passStr);
      //print(p);
      passports.add(p);
      //print("-------------");
    });

    print(passports.where((element) => element.isValid()).length);
  });
}

class Passport {
  static const String BYR = "byr";
  static const String IYR = "iyr";
  static const String EYR = "eyr";
  static const String HGT = "hgt";
  static const String HCL = "hcl";
  static const String ECL = "ecl";
  static const String PID = "pid";
  static const String CID = "cid";

  int? byr;
  int? iyr;
  int? eyr;
  int? hgt;
  String? hcl;
  String? ecl;
  String? pid;
  String? cid;

  Passport();

  factory Passport.fromInput(String input) {
    Passport passport = Passport();
    List<String> datas = input.split(' ');
    datas.forEach((line) {
      String type = line.split(":")[0];
      String data = line.split(":")[1].trim();
      switch (type) {
        case BYR:
          passport.byr = TypeUtils.checkYear(data, 1920, 2002);
          break;
        case IYR:
          passport.iyr = TypeUtils.checkYear(data, 2010, 2020);
          break;
        case EYR:
          passport.eyr = TypeUtils.checkYear(data, 2020, 2030);
          break;
        case HGT:
          passport.hgt = TypeUtils.checkHeight(data, 150, 193, 59, 76);
          break;
        case ECL:
          passport.ecl = TypeUtils.checkColor(data);
          break;
        case HCL:
          passport.hcl = TypeUtils.checkColorHexa(data);
          break;
        case PID:
          passport.pid = TypeUtils.checkDigits(data);
          break;
        case CID:
          passport.cid = data;
          break;
      }
    });

    return passport;
  }

  bool isValid() {
    return byr != null &&
        iyr != null &&
        eyr != null &&
        hgt != null &&
        ecl != null &&
        hcl != null &&
        pid != null;
  }

  String toString() {
    return "$BYR:$byr $IYR:$iyr $EYR:$eyr $HGT:$hgt $ECL:$ecl $HCL:$hcl $PID:$pid $CID:$cid \n VALID:${this.isValid()}";
  }
}

class TypeUtils {
  static int? checkNumber(String data, int min, int max) {
    int? year = int.tryParse(data, radix: 10);
    if (year == null) {
      return null;
    }
    return year >= min && year <= max ? year : null;
  }

  static int? checkYear(String data, int min, int max) {
    return checkNumber(data, min, max);
  }

  static int? checkHeight(
      String data, int minCm, int maxCm, int minIn, int maxIn) {
    bool isCm = data.contains("cm");
    bool isIn = data.contains("in");

    if (!isCm && !isIn) {
      return null;
    }
    if (isCm) {
      data = data.replaceAll("cm", "");
      return checkNumber(data, minCm, maxCm);
    } else if (isIn) {
      data = data.replaceAll("in", "");
      return checkNumber(data, minIn, maxIn);
    }
  }

  static String? checkColor(String data) {
    List<String> colors = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
    return colors.contains(data) ? data : null;
  }

  static String? checkColorHexa(String data) {
    RegExp reg = RegExp(r'^#([a-fA-F0-9]{6})$');
    return reg.stringMatch(data) == data ? data : null;
  }

  static String? checkDigits(String data) {
    RegExp reg = RegExp(r'^([0-9]{9})$');
    return reg.stringMatch(data) == data ? data : null;
  }
}
