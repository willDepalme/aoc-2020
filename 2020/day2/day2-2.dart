import 'dart:io';

main(List<String> args) {
  File input = new File("input");
  input.readAsLines().then((lines) {
    int nbValidPwd = lines.where((element) {
      var parts = element.split(':');
      var policy = Policy.fromString(parts[0]);
      return policy.isPasswordValid(parts[1]);
    }).length;
    print(nbValidPwd);
  });
}

class Policy {
  int firstPos;
  int lastPos;
  String letter;

  Policy({required this.firstPos, required this.lastPos, required this.letter});

  factory Policy.fromString(String policy) {
    String minMax = policy.split(" ")[0];
    int min = int.parse(minMax.split('-')[0]);
    int max = int.parse(minMax.split('-')[1]);
    String letter = policy.split(" ")[1];
    return Policy(firstPos: min, lastPos: max, letter: letter);
  }

  bool isPasswordValid(String password) {
    return (password[this.firstPos] == letter ||
            password[this.lastPos] == letter) &&
        password[this.firstPos] != password[this.lastPos];
  }
}
