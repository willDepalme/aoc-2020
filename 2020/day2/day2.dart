import 'dart:io';

main(List<String> args) {
  File input = new File("input");
  input.readAsLines().then((lines) {
    int nbValidPwd = lines.where((element) {
      var parts = element.split(':');
      var policy = Policy.fromString(parts[0]);
      return policy.isPasswordValid(parts[1]);
    }).length;
    print(nbValidPwd);
  });
}

class Policy {
  int min;
  int max;
  String letter;

  Policy({required this.min, required this.max, required this.letter});

  factory Policy.fromString(String policy) {
    String minMax = policy.split(" ")[0];
    int min = int.parse(minMax.split('-')[0]);
    int max = int.parse(minMax.split('-')[1]);
    String letter = policy.split(" ")[1];
    return Policy(min: min, max: max, letter: letter);
  }

  bool isPasswordValid(String password) {
    var cpt = 0;
    for (int i = 0; i < password.length; i++) {
      if (password[i] == this.letter) cpt++;
    }
    return this.min <= cpt && cpt <= this.max;
  }
}
