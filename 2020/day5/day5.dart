import 'dart:io';

const int MAX_ROW = 127;
const int MAX_COL = 7;

main(List<String> args) {
  File input = File("input");
  int maxSeatID = 0;

  input.readAsLines().then((lines) {
    lines.forEach((element) {
      int seatId = calcSeatId(element);
      if (seatId > maxSeatID) {
        maxSeatID = seatId;
      }
    });
    print(maxSeatID);
  });
}

int calcSeatId(String code) {
  int seatID = 0;
  int rowMin = 0, rowMax = MAX_ROW;
  int colMin = 0, colMax = MAX_COL;

  for (int i = 0; i < code.length; i++) {
    String letter = code[i];
    if (isRowCode(letter)) {
      if (letter == "F") {
        rowMax = (rowMax - ((rowMax - rowMin) / 2).round());
      } else {
        rowMin = (rowMin + ((rowMax - rowMin) / 2).round());
      }
      //print("Row between $rowMin - $rowMax");
    } else {
      if (letter == "L") {
        colMax = (colMax - ((colMax - colMin) / 2).round());
      } else {
        colMin = (colMin + ((colMax - colMin) / 2).round());
      }
      //print("Col between $colMin - $colMax");
    }
  }

  seatID = rowMin * 8 + colMin;
  //print("SeatId $seatID");
  return seatID;
}

bool isRowCode(String letter) {
  return letter == "F" || letter == "B";
}

bool isColCode(String letter) {
  return letter == "L" || letter == "R";
}
