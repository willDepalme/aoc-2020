import 'dart:io';

const int MAX_ROW = 127;
const int MAX_COL = 7;

main(List<String> args) {
  File input = File("input");
  Set<int> availableSeats = initAvailableSeats();
  Set<int> boardingSeats = Set<int>();

  input.readAsLines().then((lines) {
    lines.forEach((element) {
      int seatID = getSeat(element);
      availableSeats.remove(seatID);
      boardingSeats.add(seatID);
    });

    availableSeats = availableSeats.where((element) {
      bool previousSeat = boardingSeats.contains(element - 1);
      bool nextSeat = boardingSeats.contains(element + 1);
      return previousSeat && nextSeat;
    }).toSet();

    availableSeats.forEach((element) {
      print(element);
    });
  });
}

Set<int> initAvailableSeats() {
  Set<int> availableSeats = Set<int>();
  for (int i = 1; i < MAX_ROW; i++) {
    for (int j = 0; j <= MAX_COL; j++) {
      int seatId = i * 8 + j;
      availableSeats.add(seatId);
    }
  }
  return availableSeats;
}

int getSeat(String code) {
  int seatID = 0;
  int rowMin = 0, rowMax = MAX_ROW;
  int colMin = 0, colMax = MAX_COL;

  for (int i = 0; i < code.length; i++) {
    String letter = code[i];
    if (isRowCode(letter)) {
      if (letter == "F") {
        rowMax = (rowMax - ((rowMax - rowMin) / 2).round());
      } else {
        rowMin = (rowMin + ((rowMax - rowMin) / 2).round());
      }
      //print("Row between $rowMin - $rowMax");
    } else {
      if (letter == "L") {
        colMax = (colMax - ((colMax - colMin) / 2).round());
      } else {
        colMin = (colMin + ((colMax - colMin) / 2).round());
      }
      //print("Col between $colMin - $colMax");
    }
  }

  seatID = rowMin * 8 + colMin;
  return seatID;
}

bool isRowCode(String letter) {
  return letter == "F" || letter == "B";
}

bool isColCode(String letter) {
  return letter == "L" || letter == "R";
}
