import 'dart:io';

main(List<String> args) {
  File input = File("input");

  int answersCpt = 0;
  input.readAsStringSync().split("\n\n").forEach((group) {
    answersCpt += readGroup(group);
  });
  print(answersCpt);
}

int readGroup(String group) {
  Set<String> answers = Set<String>();
  group.split("\n").forEach((person) {
    readPerson(person, answers);
  });
  //print(answers);
  return answers.length;
}

void readPerson(String person, Set<String> answers) {
  for (int i = 0; i < person.length; i++) {
    answers.add(person[i]);
  }
}
