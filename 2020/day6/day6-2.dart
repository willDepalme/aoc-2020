import 'dart:io';

main(List<String> args) {
  File input = File("input");

  int commonAnswersCpt = 0;
  input.readAsStringSync().split("\n\n").forEach((group) {
    commonAnswersCpt += getCommonAnwsersOfGroup(group.trim());
  });
  print(commonAnswersCpt);
}

int getCommonAnwsersOfGroup(String group) {
  List<String> persons = group.split("\n");
  String allAnwsers = persons.join("");
  Set<String> commonAnswers = new Set<String>();

  for (int i = 0; i < allAnwsers.length; i++) {
    String ans = allAnwsers[i];
    if (!commonAnswers.contains(ans)) {
      String tmp = allAnwsers + "";
      tmp = tmp.replaceAll(ans, "");
      if (allAnwsers.length - tmp.length == persons.length) {
        commonAnswers.add(ans);
      }
    }
  }
  // print(commonAnswers);
  return commonAnswers.length;
}
