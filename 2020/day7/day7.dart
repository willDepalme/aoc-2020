import 'dart:io';

main(List<String> args) {
  File input = File("input");

  Map<String, Bag> parsedBags = {};

  // Parsed bag input
  List<String> lines = input.readAsLinesSync();
  //print("${lines.length} lines");
  lines.forEach((line) {
    Bag bag = parseBag(line);
    parsedBags[bag.color] = bag;
  });

  // Find bags that directly contains shiny gold
  List<String> bagsCanContainShiny = parsedBags.entries
      .where((element) => element.value.containShiny)
      .map((e) => e.key)
      .toList();
  parsedBags.removeWhere((key, value) => bagsCanContainShiny.contains(key));

  // Loop again and again in the bags to check if bag contains bag that contains shiny gold
  int bagsAdded = bagsCanContainShiny.length;
  while (bagsAdded > 0) {
    bagsAdded = 0;
    parsedBags.forEach((key, value) {
      var colorsLists = [
        bagsCanContainShiny,
        value.bags.map((e) => e.color).toList()
      ];
      final commonColors = colorsLists.fold<Set>(
          colorsLists.first.toSet(), (a, b) => a.intersection(b.toSet()));
      if (commonColors.isNotEmpty &&
          !bagsCanContainShiny.contains(value.color)) {
        bagsCanContainShiny.add(value.color);
        bagsAdded++;
      }
    });

    parsedBags.removeWhere((key, value) => bagsCanContainShiny.contains(key));
  }

  print(bagsCanContainShiny.length);
}
// mirrored turquoise
// light maroon
// shiny black
// mirrored tomato

Bag parseBag(String line) {
  String bagColor = line.split("contain")[0].replaceAll("bags", "").trim();
  List<Bag> bags = [];
  bool containShiny = false;
  if (line.split("contain")[1].trim() != "no other bags.") {
    line.split("contain")[1].trim().split(",").forEach((bagStr) {
      String color = bagStr
          .replaceAll("bags", "")
          .replaceAll("bag", "")
          .replaceAll(".", "")
          .trim();
      color = color.substring(color.indexOf(" ") + 1);
      if (color == "shiny gold") {
        containShiny = true;
      }
      bags.add(Bag(color: color, bags: [], containShiny: false));
    });
  }

  return Bag(color: bagColor, bags: bags, containShiny: containShiny);
}

void printBagTree(Bag bag, int level) {
  String tabs = "\t-" * level;
  if (bag.color == "shiny gold") {
    print(tabs + ("&&" + bag.color + "&&").toUpperCase());
  } else {
    print(tabs + bag.color);
  }
  bag.bags.forEach((element) {
    printBagTree(element, level + 1);
  });
}

class Bag {
  String color;
  bool containShiny;
  List<Bag> bags = [];

  Bag({required this.color, required this.bags, required this.containShiny});

  bool canContainShiny() {
    if (this.containShiny) {
      return true;
    } else {
      if (bags.isEmpty) {
        return this.containShiny;
      } else {
        bool ccs = false;
        for (int i = 0; i < bags.length; i++) {
          if (bags[i].canContainShiny()) {
            ccs = true;
            continue;
          }
        }
        return ccs;
      }
    }
  }
}
