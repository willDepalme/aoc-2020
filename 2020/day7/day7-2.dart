import 'dart:io';

main(List<String> args) {
  File input = File("input");

  Map<String, Bag> parsedBags = {};

  // Parsed bag input
  List<String> lines = input.readAsLinesSync();
  lines.forEach((line) {
    Bag bag = parseBag(line);
    parsedBags[bag.color] = bag;
  });

  print(countBagsInsideBag("shiny gold", parsedBags));
}

int countBagsInsideBag(String color, Map<String, Bag> parsedBags) {
  Bag bag = parsedBags[color]!;
  if (bag.bags.isEmpty) {
    return 0;
  } else {
    int count = 0;
    bag.bags.forEach((col, cpt) {
      count += cpt + cpt * countBagsInsideBag(col, parsedBags);
    });
    return count;
  }
}

Bag parseBag(String line) {
  String bagColor = line.split("contain")[0].replaceAll("bags", "").trim();
  Map<String, int> bags = {};
  if (line.split("contain")[1].trim() != "no other bags.") {
    line.split("contain")[1].trim().split(",").forEach((bagStr) {
      String color = bagStr
          .replaceAll("bags", "")
          .replaceAll("bag", "")
          .replaceAll(".", "")
          .trim();
      int count = int.parse(color.split(" ")[0]);
      color = color.substring(color.indexOf(" ") + 1);
      bags[color] = count;
    });
  }

  return Bag(color: bagColor, bags: bags);
}

class Bag {
  String color;
  Map<String, int> bags = {};

  Bag({required this.color, required this.bags});

  String toString() {
    return "$color : $bags";
  }
}
