import 'dart:io';

main(List<String> args) {
  int linesWidth = 0;

  List<Slope> slopes = [
    Slope(right: 1, down: 1, treeCount: 0),
    Slope(right: 3, down: 1, treeCount: 0),
    Slope(right: 5, down: 1, treeCount: 0),
    Slope(right: 7, down: 1, treeCount: 0),
    Slope(right: 1, down: 2, treeCount: 0),
  ];

  File input = File("input");
  input.readAsLines().then((lines) {
    linesWidth = lines[0].length;

    slopes.forEach((slope) {
      int posX = 0;
      int posY = 0;
      do {
        posX = (posX + slope.right) % linesWidth;
        posY += slope.down;
        if (posY < lines.length) {
          if (lines[posY][posX] == '#') {
            slope.treeCount++;
          }
        }
      } while (posY < lines.length);

      print("${slope.treeCount} trees encountered");
    });

    var total = slopes
        .map((e) => e.treeCount)
        .reduce((value, element) => value * element);
    print(total);
  });
}

class Slope {
  int right;
  int down;
  late int treeCount;

  Slope({required this.right, required this.down, required this.treeCount});
}
