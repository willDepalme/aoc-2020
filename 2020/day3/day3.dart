import 'dart:io';

main(List<String> args) {
  const stepRight = 3;
  const stepDown = 1;

  int linesWidth = 0;
  int posX = 0;
  int treeCount = 0;

  File input = File("input");
  input.readAsLines().then((lines) {
    int lineIndex = 0;
    lines.forEach((line) {
      if (lineIndex == 0) {
        linesWidth = line.length;
      } else {
        posX = (posX + stepRight) % linesWidth;
        print("On case ${line[posX]}");
        if (line[posX] == '#') treeCount++;
      }
      lineIndex++;
    });

    print(treeCount);
  });
}
