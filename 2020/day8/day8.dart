import 'dart:io';

main(List<String> args) {
  File input = File("input");

  List<String> lines = input.readAsLinesSync();
  int nbLines = lines.length;

  List<int> executedLines = [];

  int pos = 0;
  int acc = 0;

  while (!executedLines.contains(pos)) {
    String line = lines[pos];
    // print(line);
    String command = line.split(" ")[0];
    int value = int.parse(line.split(" ")[1]);

    executedLines.add(pos);

    switch (command) {
      case "acc":
        acc += value;
        pos = (pos + 1) % nbLines;
        break;
      case "jmp":
        pos = (pos + value);
        if (pos >= nbLines) {
          pos -= nbLines;
        } else if (pos < 0) {
          pos = nbLines + pos;
        }
        break;
      default:
        pos++;
        break;
    }
    print("Acc : $acc,  Pos : $pos");
  }

  print(acc);
}
