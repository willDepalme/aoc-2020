import 'dart:io';

main(List<String> args) {
  File input = File("input");

  List<String> lines = input.readAsLinesSync();
  List<int> linesChanged = [];

  int? acc;
  int? lineToChange;

  while (acc == null) {
    List<String> linesCopy = []..addAll(lines);
    if (lineToChange != null) {
      print("Modifying line $lineToChange : ${linesCopy[lineToChange]}");
      String command = linesCopy[lineToChange] + "";
      if (linesCopy[lineToChange].contains("jmp")) {
        command = command.replaceAll("jmp", "nop");
      } else if (linesCopy[lineToChange].contains("nop") &&
          !linesCopy[lineToChange].contains("+0")) {
        command = command.replaceAll("nop", "jmp");
      }
      linesChanged.add(lineToChange);
      linesCopy[lineToChange] = command;
      print("New line => ${linesCopy[lineToChange]}");
    }
    try {
      acc = runLines(linesCopy, linesChanged);
      print(acc);
    } catch (alreadyExecutedError) {
      if (alreadyExecutedError.runtimeType == RunLineException) {
        lineToChange = (alreadyExecutedError as RunLineException).pos;
        print("Error on run: Line $lineToChange already executed");
      }
    }
  }
}

int runLines(List<String> lines, List<int> linesChanged) {
  List<int> executedLines = [];
  int nbLines = lines.length;

  int pos = 0;
  int acc = 0;

  while (pos < nbLines) {
    String line = lines[pos];
    //print(line);
    String command = line.split(" ")[0];
    int value = int.parse(line.split(" ")[1]);

    if (executedLines.contains(pos)) {
      if ((command == "jmp" || command == "nop") &&
          !linesChanged.contains(pos)) {
        throw RunLineException(pos: pos);
      }
    }

    if ((command == "jmp" || command == "nop") &&
        executedLines.contains(pos) &&
        !linesChanged.contains(pos)) {
      throw RunLineException(pos: pos);
    }
    executedLines.add(pos);

    switch (command) {
      case "acc":
        acc += value;
        pos = (pos + 1) == nbLines ? pos + 1 : (pos + 1) % nbLines;
        break;
      case "jmp":
        pos = (pos + value);
        if (pos >= nbLines) {
          pos -= nbLines;
        } else if (pos < 0) {
          pos = nbLines + pos;
        }
        break;
      default:
        pos++;
        break;
    }
    print("Acc : $acc,  Pos : $pos");
  }

  return acc;
}

class RunLineException implements Exception {
  late int pos;

  RunLineException({required this.pos});
}
