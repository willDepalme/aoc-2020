import 'dart:io';

import 'dart:math';

const int PREAMBLE = 25;

main(List<String> args) {
  File input = File("input");
  List<String> lines = input.readAsLinesSync();

  List<int> numbers = lines.map((e) => int.parse(e)).toList();

  for (int i = PREAMBLE; i < numbers.length; i++) {
    print("Line $i : ${numbers[i]}");
    bool valid = isValidNumber(numbers, i);
    if (!valid) {
      print("NON VALID");
      return;
    }
  }
}

bool isValidNumber(List<int> numbers, int pos) {
  int start = max(0, pos - PREAMBLE);
  List<int> prevNumbers = numbers.sublist(start, pos + 1);

  List<int> sums = [];

  prevNumbers.forEach((n) {
    List<int> subList = prevNumbers.toList();
    subList.remove(n);
    sums.addAll(subList.map((e) => e + n).toList());
  });

  //print(sums);
  return sums.contains(numbers[pos]);
}
