import 'dart:io';

import 'dart:math';

const int KEY = 3199139634;

main(List<String> args) {
  File input = File("input");

  List<int> numbers = input.readAsLinesSync().map((e) => int.parse(e)).toList();

  for (int i = numbers.length - 1; i > 0; i--) {
    int sum = 0;
    int pos = 0;
    List<int> subList = [];
    while (sum <= KEY) {
      pos = max(i - pos, 0);
      print(pos);
      sum += numbers[pos];
      subList.add(numbers[pos]);
      pos++;
    }
    if (sum == KEY) {
      print(subList);
      subList.sort();
      print(
          "${subList.first} + ${subList.last} = ${subList.first + subList.last}");
    }
  }
}
