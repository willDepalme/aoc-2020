import 'dart:io';

main(List<String> args) {
  File input = new File("input");
  input.readAsLines().then((lines) {
    for (int i = 0; i < lines.length; i++) {
      for (int j = 0; j < lines.length; j++) {
        if (j != i) {
          if (int.parse(lines[i]) + int.parse(lines[j]) == 2020) {
            print(int.parse(lines[i]) * int.parse(lines[j]));
            return;
          }
        }
      }
    }
  });
}
