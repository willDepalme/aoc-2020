import 'dart:io';

int nbPath = 0;

main(List<String> args) {
  File input = new File("input");
  List<String> lines = input.readAsLinesSync();

  List<Cave> caves = [];

  // Create nodes
  lines.forEach((line) {
    String cave1Id = line.split("-")[0];
    String cave2Id = line.split("-")[1];

    Cave cave1 = caves.firstWhere((c) => c.id == cave1Id, orElse: () {
      Cave tmpCave = Cave(cave1Id, cave1Id.toLowerCase() == cave1Id);
      caves.add(tmpCave);
      return tmpCave;
    });

    Cave cave2 = caves.firstWhere((c) => c.id == cave2Id, orElse: () {
      Cave tmpCave = Cave(cave2Id, cave2Id.toLowerCase() == cave2Id);
      caves.add(tmpCave);
      return tmpCave;
    });

    cave1.addLinkedCave(cave2);
  });

  // Rearrange node in tree
  caves.forEach(print);
  print("\n\n");

  Cave start = caves.firstWhere((c) => c.id == "start");
  checkPathsToEnd(start, []);

  print("\n $nbPath paths found");
}

void checkPathsToEnd(Cave start, List<Cave> path) {
  if (start.small) {
    bool containsSmallCaveVisitedTwice = false;
    String twiceCaveId = "";
    path.where((c) => c.small).forEach((cave) {
      int count = path.where((c) => c.id == cave.id).length;
      if (count > 1) {
        containsSmallCaveVisitedTwice = true;
        twiceCaveId == cave.id;
        //print("Small cave ${cave.id} already visited twice");
        //print(path.map((e) => e.id).join(","));
      }
    });

    if ((containsSmallCaveVisitedTwice && start.id == twiceCaveId) ||
        (containsSmallCaveVisitedTwice && path.any((c) => c.id == start.id))) {
      return; // Cannot visit small cave twice
    }
  }

  path.add(start);
  if (start.id == "end") {
    print(path.map((e) => e.id).join(","));
    nbPath++;
  } else {
    start.linked.where((c) => c.id != "start").forEach((cave) {
      checkPathsToEnd(cave, []..addAll(path));
    });
  }
}

/**
 *     
 *    start
      /   \
  c--A-----b--d
      \   /
       end
 */

/**
 * Used as node
 */
class Cave {
  String id;
  bool small;
  List<Cave> linked = [];

  Cave(this.id, this.small);

  void addLinkedCave(Cave cave) {
    if (!this.linked.any((c) => c.id == cave.id)) {
      this.linked.add(cave);
      cave.addLinkedCave(this);
    }
  }

  String toString() {
    return "Cave $id linked to [${linked.map((e) => e.id).join(",")}]";
  }
}
