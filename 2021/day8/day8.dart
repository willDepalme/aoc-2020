import 'dart:io';

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  int total = 0;
  lines.forEach((line) {
    total += parseLine(line);
  });
  print(total);
}

/**
   aaaa    ....    aaaa    aaaa    ....
  b    c  .    c  .    c  .    c  b    c
  b    c  .    c  .    c  .    c  b    c
   ....    ....    dddd    dddd    dddd
  e    f  .    f  e    .  .    f  .    f
  e    f  .    f  e    .  .    f  .    f
   gggg    ....    gggg    gggg    ....
 */
int parseLine(String line) {
  List<String> signals = line.split(" | ")[0].split(" ");
  List<String> outputs = line.split(" | ")[1].split(" ");
  //print(outputs);

  Map<String, String> convertSegment = {
    "a": "",
    "b": "",
    "c": "",
    "d": "",
    "e": "",
    "f": "",
    "g": ""
  };

  String oneSignal = signals.where((s) => s.length == 2).first;
  String sevenSignal = signals.where((s) => s.length == 3).first;
  String fourSignal = signals.where((s) => s.length == 4).first;
  String eightSignal = signals.where((s) => s.length == 7).first;

  int count = outputs.where((o) => [2, 3, 4, 7].contains(o.length)).length;
  //print(count);
  return count;
}
