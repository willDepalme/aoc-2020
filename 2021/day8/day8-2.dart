import 'dart:io';

const segmentsToNumber = {
  "abcefg": "0",
  "cf": "1",
  "acdeg": "2",
  "acdfg": "3",
  "bcdf": "4",
  "abdfg": "5",
  "abdefg": "6",
  "acf": "7",
  "abcdefg": "8",
  "abcdfg": "9"
};

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  int total = 0;
  lines.forEach((line) {
    total += parseLine(line);
  });
  print("Total = $total");
}

/**
   aaaa    ....    aaaa    aaaa    ....
  b    c  .    c  .    c  .    c  b    c
  b    c  .    c  .    c  .    c  b    c
   ....    ....    dddd    dddd    dddd
  e    f  .    f  e    .  .    f  .    f
  e    f  .    f  e    .  .    f  .    f
   gggg    ....    gggg    gggg    ....

   aaaa    aaaa    aaaa    aaaa    aaaa
  b    .  b    .  .    c  b    c  b    c
  b    .  b    .  .    c  b    c  b    c
   dddd    dddd    ....    dddd    dddd
  .    f  e    f  .    f  e    f  .    f
  .    f  e    f  .    f  e    f  .    f
   gggg    gggg    ....    gggg    gggg

   a 8
   b 6
   c 8
   d 7
   e 4
   f 9
   g 7
 */
int parseLine(String line) {
  List<String> split = line.split(" | ");
  List<String> outputs = split[1].split(" ");

  // Map: signal letter -> orignal letter
  Map<String, String> map = generateSegmentMap(split[0]);
  String number = "";
  outputs.forEach((output) {
    number += getNumber(output, map);
  });
  print(split[1] + " => " + number);
  return int.parse(number);
}

Map<String, String> generateSegmentMap(String signalLine) {
  //print(signalLine);
  Map<String, String> map = {
    "a": "",
    "b": "",
    "c": "",
    "d": "",
    "e": "",
    "f": "",
    "g": ""
  };

  List<String> signals = signalLine.split(" ");
  if (signals.length != 10) {
    print("SIGNAL INPUT NOT CORRECT");
  }

  Map<String, int> charFrequency = {};
  int length = signalLine.length;
  for (var letter in ["a", "b", "c", "d", "e", "f", "g"]) {
    charFrequency[letter] = length - signalLine.replaceAll(letter, "").length;
  }
  //print(charFrequency);

  charFrequency.forEach((key, value) {
    if (value == 4) {
      map["e"] = key;
    } else if (value == 6) {
      map["b"] = key;
    } else if (value == 9) {
      map["f"] = key;
    }
  });

  String oneSignal = signals.where((s) => s.length == 2).first;
  String sevenSignal = signals.where((s) => s.length == 3).first;
  String fourSignal = signals.where((s) => s.length == 4).first;

  map["a"] =
      sevenSignal.split("").where((seg) => !oneSignal.contains(seg)).first;

  map["d"] = fourSignal
      .split("")
      .where((seg) => !oneSignal.contains(seg) && seg != map["b"])
      .first;

  map["c"] = oneSignal.replaceAll(map["f"]!, "");

  String lastLetterG = "abcdefg";
  for (String letter in lastLetterG.split("")) {
    lastLetterG = lastLetterG.replaceAll(map[letter]!, "");
  }
  map["g"] = lastLetterG;

  //print(map);
  return revertMap(map);
}

Map<String, String> revertMap(Map<String, String> map) {
  Map<String, String> reverted = {};

  map.forEach((key, value) {
    reverted[value] = key;
  });

  return reverted;
}

String getNumber(String output, Map<String, String> map) {
  String fixedOutput = "";
  output.split("").forEach((letter) {
    fixedOutput += map[letter]!;
  });

  List<String> ordered = fixedOutput.split("");
  ordered.sort();
  try {
    String number = segmentsToNumber[ordered.join("")]!;
    // print(output + " => " + ordered.join() + " => " + number);
    return number;
  } catch (err) {
    print("Cannot map ${ordered.join("")} to a number");
  }
  return "x";
}
