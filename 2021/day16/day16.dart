import 'dart:io';

Map<String, String> hexa = {
  "0": "0000",
  "1": "0001",
  "2": "0010",
  "3": "0011",
  "4": "0100",
  "5": "0101",
  "6": "0110",
  "7": "0111",
  "8": "1000",
  "9": "1001",
  "A": "1010",
  "B": "1011",
  "C": "1100",
  "D": "1101",
  "E": "1110",
  "F": "1111"
};

int totalVersion = 0;

main() {
  File input = File("input");

  //String content = "38006F45291200"; // Op total length
  //String content = "EE00D40C823060"; // Op nb sub packets
  //String content = "8A004A801A8002F478"; // sum 16
  //String content = "620080001611562C8802118E34"; // sum 12
  //String content = "C0015000016115A2E0802F182340"; // sum 23
  // String content = "A0016C880162017C3686B18A3D4780"; // sum 31
  String content = input.readAsStringSync();

  print(content);

  String byteCode = content.split("").map((e) => hexa[e]).join("");
  print("$byteCode (${byteCode.length})");

  Pointer startIndex = Pointer(0);
  readPackage(byteCode, startIndex, 0);
  print("Sum version : $totalVersion");
}

void readPackage(String byteCode, Pointer startIndex, int level) {
  String tabs = "\t-" * level;
  print(tabs + "READ PACKAGE");
  int version = parseVersion(byteCode, startIndex);
  totalVersion += version;
  int typeId = parseTypeId(byteCode, startIndex);
  print(tabs + "Vers: $version, TypeID: $typeId");
  if (typeId == 4) {
    bool lastRead = false;
    while (!lastRead) {
      lastRead = byteCode[startIndex.index] == "0";
      int value = parseSubPacketValue(byteCode, startIndex);
      print(tabs + "value : $value");
    }
  } else {
    print(tabs + "Operator type");
    String lengthTypeID = getLengthTypeId(byteCode, startIndex);
    if (lengthTypeID == "0") {
      int totalLength = parseTotalLength(byteCode, startIndex);
      int subIndex = startIndex.index + 0;
      print(tabs + "$totalLength bits to read");
      //String finalBytes = getBytesFromLength(byteCode, startIndex, totalLength);
      while (startIndex.index < subIndex + totalLength) {
        readPackage(byteCode, startIndex, level + 1);
      }
      print(tabs + "End TotalLength");
    } else {
      int nbSubPacket = parseNbSubPackets(byteCode, startIndex);
      print(tabs + "$nbSubPacket sub-packets to read ");
      for (int i = 0; i < nbSubPacket; i++) {
        readPackage(byteCode, startIndex, level + 1);
      }
    }
  }
  print(tabs + "---- pos ${startIndex.index} ----");
}

int parseVersion(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 3), radix: 2);
  p.index += 3;
  return val;
}

int parseTypeId(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 3), radix: 2);
  p.index += 3;
  return val;
}

String getLengthTypeId(String input, Pointer p) {
  String val = input[p.index];
  p.index++;
  return val;
}

int parseTotalLength(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 15), radix: 2);
  p.index += 15;
  return val;
}

int parseNbSubPackets(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 11), radix: 2);
  p.index += 11;
  return val;
}

int parseSubPacketValue(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 5), radix: 2);
  p.index += 5;
  return val;
}

String getBytesFromLength(String input, Pointer p, int length) {
  String val = input.substring(p.index, p.index + length);
  p.index += length;
  return val;
}

class Pointer {
  int index;

  Pointer(this.index);
}
