import 'dart:io';

Map<String, String> hexa = {
  "0": "0000",
  "1": "0001",
  "2": "0010",
  "3": "0011",
  "4": "0100",
  "5": "0101",
  "6": "0110",
  "7": "0111",
  "8": "1000",
  "9": "1001",
  "A": "1010",
  "B": "1011",
  "C": "1100",
  "D": "1101",
  "E": "1110",
  "F": "1111"
};

main() {
  File input = File("input");

  //String content = "C200B40A82"; // 3
  //String content = "04005AC33890"; // 54
  //String content = "880086C3E88112"; // 7
  //String content = "CE00C43D881120"; // 9
  //String content = "D8005AC2A8F0"; // 1
  //String content = "F600BC2D8F"; // 0
  //String content = "9C005AC2F8F0"; // 0
  //String content = "9C0141080250320F1802104A08"; // 1
  //String content = "38006F45291200";
  String content = input.readAsStringSync();

  print(content);

  String byteCode = content.split("").map((e) => hexa[e]).join("");
  print("$byteCode (${byteCode.length})");

  Pointer startIndex = Pointer(0);
  Package root = readPackage(byteCode, startIndex, 0);

  print("\nEstimating total value");
  print("Total value : ${root.getValue(0)}");
}

Package readPackage(String byteCode, Pointer startIndex, int level) {
  String tabs = "\t-" * level;
  print(tabs + "READ PACKAGE");

  int version = parseVersion(byteCode, startIndex);
  int typeId = parseTypeId(byteCode, startIndex);
  Package package = Package(version, typeId);
  print(tabs + "Vers: $version, TypeID: $typeId");

  if (typeId == 4) {
    bool lastRead = false;
    String bytes = "";
    while (!lastRead) {
      lastRead = byteCode[startIndex.index] == "0";
      startIndex.index++;
      bytes += parseSubPacketValue(byteCode, startIndex);
    }
    int value = int.parse(bytes, radix: 2);
    print(tabs + "\t $bytes -> $value");
    package.values.add(value);
  } else {
    print(tabs + "Operator type");
    String lengthTypeID = getLengthTypeId(byteCode, startIndex);
    if (lengthTypeID == "0") {
      int totalLength = parseTotalLength(byteCode, startIndex);
      int subIndex = startIndex.index + 0;
      print(tabs + "$totalLength bits to read");
      //String finalBytes = getBytesFromLength(byteCode, startIndex, totalLength);
      while (startIndex.index < subIndex + totalLength) {
        package.subPackages.add(readPackage(byteCode, startIndex, level + 1));
      }
      print(tabs + "End TotalLength");
    } else {
      int nbSubPacket = parseNbSubPackets(byteCode, startIndex);
      print(tabs + "$nbSubPacket sub-packets to read ");
      for (int i = 0; i < nbSubPacket; i++) {
        package.subPackages.add(readPackage(byteCode, startIndex, level + 1));
      }
    }
  }
  print(tabs + "---- pos ${startIndex.index} ----");
  return package;
}

int parseVersion(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 3), radix: 2);
  p.index += 3;
  return val;
}

int parseTypeId(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 3), radix: 2);
  p.index += 3;
  return val;
}

String getLengthTypeId(String input, Pointer p) {
  String val = input[p.index];
  p.index++;
  return val;
}

int parseTotalLength(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 15), radix: 2);
  p.index += 15;
  return val;
}

int parseNbSubPackets(String input, Pointer p) {
  int val = int.parse(input.substring(p.index, p.index + 11), radix: 2);
  p.index += 11;
  return val;
}

String parseSubPacketValue(String input, Pointer p) {
  String val = input.substring(p.index, p.index + 4);
  p.index += 4;
  return val;
}

String getBytesFromLength(String input, Pointer p, int length) {
  String val = input.substring(p.index, p.index + length);
  p.index += length;
  return val;
}

class Pointer {
  int index;

  Pointer(this.index);
}

class Package {
  int version;
  int typeId;
  List<int> values = [];
  List<Package> subPackages = [];

  Package(this.version, this.typeId);

  int getValue(int level) {
    String tabs = "\t" * level;

    switch (typeId) {
      case 0: // Sum
        List<int> subVal =
            subPackages.map((e) => e.getValue(level + 1)).toList();
        int sum = subVal.reduce((value, element) => value + element);
        print(tabs + "$subVal sum = $sum");
        return sum;
      case 1: // Product
        List<int> subVal =
            subPackages.map((e) => e.getValue(level + 1)).toList();
        int product = subVal.fold(1, (value, element) => value * element);
        print(tabs + "$subVal product = $product");
        return product;
      case 2: // Min
        List<int> value =
            subPackages.map((e) => e.getValue(level + 1)).toList();
        value.sort();
        print(tabs + "$value min = ${value.first}");
        return value.first;
      case 3: // Max
        List<int> value =
            subPackages.map((e) => e.getValue(level + 1)).toList();
        value.sort();
        print(tabs + "$value max = ${value.last}");
        return value.last;
      case 5: // greater than
        int firstValue = subPackages[0].getValue(level + 1);
        int secondValue = subPackages[1].getValue(level + 1);
        return firstValue > secondValue ? 1 : 0;
      case 6: // Less than
        int firstValue = subPackages[0].getValue(level + 1);
        int secondValue = subPackages[1].getValue(level + 1);
        return firstValue < secondValue ? 1 : 0;
      case 7: // equals
        int firstValue = subPackages[0].getValue(level + 1);
        int secondValue = subPackages[1].getValue(level + 1);
        return firstValue == secondValue ? 1 : 0;
    }
    return values[0];
  }
}


// 274386279057754
// 274386279079761
/*

001 110 0 000000000011011 110 100 01010 010 100 1000100100 0000000
VVV TTT I LLLLLLLLLLLLLLL VVV TTT AAAAA VVV TTT BBBBB BBBBB
 1   6  0       27         6   4    10   2   4   17     4

  010-100-10001|00100
   2   4   17     4
*/