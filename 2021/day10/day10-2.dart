import 'dart:io';

const List<String> openings = ["{", "(", "[", "<"];
const Map<String, String> closing = {"{": "}", "(": ")", "[": "]", "<": ">"};
const Map<String, int> scorePoints = {"}": 3, ")": 1, "]": 2, ">": 4};

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  List<int> scores = [];

  lines.forEach((element) {
    int res = checkLineSyntax(element);
    if (res != 0) {
      scores.add(res);
    }
  });
  scores.sort();
  print(scores);

  int middleIndex = (scores.length / 2).floor();
  print("Middle score => ${scores[middleIndex]}");
}

int checkLineSyntax(String line) {
  int score = 0;

  print("\nCheck line $line");
  List<String> stack = [];
  for (String symbol in line.split("")) {
    if (openings.contains(symbol)) {
      stack.add(symbol);
    } else if (symbol != closing[stack.last]!) {
      print("Error : expected ${closing[stack.last]!} symbol");
      return 0; // Corrupted
    } else {
      stack.removeLast();
    }
  }

  String complete = "";
  for (String symbol in stack.reversed) {
    complete += closing[symbol]!;
    score = (score * 5) + scorePoints[closing[symbol]!]!;
  }
  print("Adding $complete with score $score");

  return score;
}

bool isIncomplete(String line) {
  for (String symbol in openings) {
    int nbOpening = line.length - line.replaceAll(symbol, '').length;
    int nbClosing = line.length - line.replaceAll(closing[symbol]!, '').length;
    print("$nbOpening opening '$symbol' and $nbClosing '${closing[symbol]!}'");
    if (nbOpening != nbClosing) {
      return true;
    }
  }
  return false;
}
