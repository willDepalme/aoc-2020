import 'dart:io';

const List<String> openings = ["{", "(", "[", "<"];
const Map<String, String> closing = {"{": "}", "(": ")", "[": "]", "<": ">"};
const Map<String, int> errorPoints = {"}": 1197, ")": 3, "]": 57, ">": 25137};

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  int totalError = 0;

  lines.forEach((element) {
    totalError += checkLineSyntax(element);
  });

  print("Total error points : $totalError");
}

int checkLineSyntax(String line) {
  if (isIncomplete(line)) {
    return 0;
  }

  var stack = [];

  for (String symbol in line.split("")) {
    // print(symbol);
    if (openings.contains(symbol)) {
      stack.add(symbol);
    } else {
      if (stack.isEmpty) {
        return errorPoints[symbol]!;
      } else if (symbol != closing[stack.last]!) {
        print("Error : expected ${closing[stack.last]!} symbol");
        return errorPoints[symbol]!;
      } else {
        stack.removeLast();
      }
    }
  }

  return 0;
}

bool isIncomplete(String line) {
  for (String symbol in openings) {
    if (line.allMatches(symbol).length !=
        line.allMatches(closing[symbol]!).length) {
      return true;
    }
  }
  return false;
}
