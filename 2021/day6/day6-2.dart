import 'dart:io';

main(List<String> args) {
  File input = new File("input");
  const int nbDays = 256;

  List<String> lines = input.readAsLinesSync();

  List<int> fishs = lines[0].split(",").map((e) => int.parse(e)).toList();
  Map<int, int> fishByStatus = {};

  // init data
  for (int i = 0; i < 9; i++) {
    fishByStatus[i] = fishs.where((e) => e == i).length;
  }
  print("Initial day");
  print(fishByStatus);

  for (int day = 1; day <= nbDays; day++) {
    //print("Day $day");
    Map<int, int> newMap = {8: 0};
    for (int i = 8; i >= 0; i--) {
      if (i == 0) {
        newMap[8] = fishByStatus[0]!; // add new born
        newMap[6] = newMap[6]! + fishByStatus[0]!; // reset fishs giving birth
      } else {
        newMap[i - 1] = fishByStatus[i]!;
      }
      //print(newMap);
    }
    fishByStatus = newMap;
    //print(fishByStatus);
  }

  print(fishByStatus);
  int totalCount =
      fishByStatus.entries.map((e) => e.value).reduce((c1, c2) => c1 + c2);

  print("$totalCount after $nbDays days");
}
