import 'dart:io';

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  List<int> fishs = lines[0].split(",").map((e) => int.parse(e)).toList();
  const int nbDays = 80;

  print("Intial state $fishs");
  for (int day = 1; day <= nbDays; day++) {
    print("Day $day");
    int size = fishs.length;

    for (int i = 0; i < size; i++) {
      fishs[i] = fishs[i] - 1;
      if (fishs[i] < 0) {
        fishs[i] = 6;
        fishs.add(8);
      }
    }

    //print("After $day days : $fishs");
  }

  print("${fishs.length} after $nbDays days");
}
