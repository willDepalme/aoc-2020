import 'dart:collection';
import 'dart:io';

main() {
  File input = File("input");

  List<String> lines = input.readAsLinesSync();

  String template = "";
  Map<String, String> rules = {};
  const int steps = 10;

  lines.forEach((line) {
    if (line.isNotEmpty && line.contains("->")) {
      rules[line.split(" -> ")[0]] = line.split(" -> ")[1];
    } else if (line.isNotEmpty && !line.contains("->")) {
      template = line;
    }
  });
  print(template);

  for (int step = 1; step <= steps; step++) {
    print("Step $step");

    // index, Letters
    Map<int, String> toInsert = getLetterToInsert(template, rules);
    template = buildNewTemplate(template, toInsert);
  }

  // find most and least common element
  findElementCount(template);
}

Map<int, String> getLetterToInsert(String template, Map<String, String> rules) {
  Map<int, String> toInsert = {};

  // Find Letter to insert
  print("Get letters to insert");
  for (int i = 1; i < template.length; i++) {
    String pair = template.substring(i - 1, i + 1);
    if (rules.containsKey(pair)) {
      if (toInsert.containsKey(i)) {
        toInsert[i] = toInsert[i]! + rules[pair]!;
      } else {
        toInsert[i] = rules[pair]!;
      }
    }
  }

  return toInsert;
}

String buildNewTemplate(String template, Map<int, String> toInsert) {
  print("Sort index map");
  SplayTreeMap<int, String> sortedByIndex =
      new SplayTreeMap.from(toInsert, (key1, key2) => key2.compareTo(key1));
  //print(sortedByIndex);

  // Build new template
  print("Insert letter in template (${template.length})");
  sortedByIndex.forEach((index, letters) {
    //print("Insert at index $index");
    String part1 = template.substring(0, index);
    String part2 = template.substring(index);
    //print("$part1 >$letters< $part2");
    template = part1 + letters + part2;
  });
  //print(template);
  return template;
}

void findElementCount(String template) {
  print("Counting element appearance...");
  Map<String, int> elementCount = {};
  for (int i = 0; i < template.length; i++) {
    if (!elementCount.containsKey(template[i])) {
      elementCount[template[i]] =
          template.length - template.replaceAll(template[i], "").length;
    }
  }
  print(elementCount);

  int max = 0;
  String maxElement = "";
  int min = 0;
  String minElement = "";

  elementCount.forEach((elem, count) {
    if (min == 0 || count < min) {
      min = count;
      minElement = elem;
    } else if (count > max) {
      max = count;
      maxElement = elem;
    }
  });
  print("Least common element : $minElement with $min apparition");
  print("Most common element : $maxElement with $max apparition");
  print("Result : $max - $min = ${max - min}");
}
