import 'dart:io';

main() {
  File input = File("input");

  List<String> lines = input.readAsLinesSync();

  String template = "";
  Map<String, String> rules = {};
  const int steps = 40;

  // Init rules
  lines.forEach((line) {
    if (line.isNotEmpty && line.contains("->")) {
      rules[line.split(" -> ")[0]] = line.split(" -> ")[1];
    } else if (line.isNotEmpty && !line.contains("->")) {
      template = line;
    }
  });
  print(template);

  Map<String, int> pairCount = initPairCount(template, rules);

  // Transform template for every step
  for (int step = 1; step <= steps; step++) {
    print("\nStep $step");
    pairCount = buildNextTemplate(pairCount, rules);
  }

  // Count appearance of each Letter
  Map<String, int> elementCount = countElement(pairCount);
  deduceCommonElement(elementCount);
}

Map<String, int> initPairCount(String template, Map<String, String> rules) {
  // Init pair map
  Map<String, int> pairCount = {};
  rules.forEach((key, value) {
    pairCount[key] = 0;
  });
  for (int i = 1; i < template.length; i++) {
    String pair = template[i - 1] + template[i];
    pairCount[pair] = pairCount[pair]! + 1;
  }
  return pairCount;
}

Map<String, int> buildNextTemplate(
    Map<String, int> pairCount, Map<String, String> rules) {
  Map<String, int> newPairCount = Map.from(pairCount);
  rules.forEach((pair, insert) {
    int initCount = pairCount[pair]!;
    if (initCount > 0) {
      //print("Pair $pair found in template");
      newPairCount[pair] = newPairCount[pair]! - initCount;
      String pairLeft = pair[0] + insert;
      String pairRight = insert + pair[1];
      newPairCount[pairLeft] = newPairCount[pairLeft]! + initCount;
      //print("Add $initCount pair $pairLeft");
      //print("Add $initCount pair $pairRight");
      newPairCount[pairRight] = newPairCount[pairRight]! + initCount;
    }
  });

  return newPairCount;
}

Map<String, int> countElement(Map<String, int> pairCount) {
  Map<String, int> elementCount = {};
  pairCount.forEach((pair, count) {
    if (!elementCount.containsKey(pair[0])) {
      elementCount[pair[0]] = 0;
    }
    if (!elementCount.containsKey(pair[1])) {
      elementCount[pair[1]] = 0;
    }

    elementCount[pair[0]] = elementCount[pair[0]]! + count;
    elementCount[pair[1]] = elementCount[pair[1]]! + count;
  });
  elementCount.forEach((el, cpt) {
    elementCount[el] = (elementCount[el]! / 2).ceil();
  });
  print("\nCount by letter");
  print(elementCount);
  return elementCount;
}

void deduceCommonElement(Map<String, int> elementCount) {
  int max = 0;
  String maxElement = "";
  int min = 0;
  String minElement = "";

  elementCount.forEach((elem, count) {
    if (min == 0 || count < min) {
      min = count;
      minElement = elem;
    } else if (count > max) {
      max = count;
      maxElement = elem;
    }
  });
  print("Least common element : $minElement with $min apparition");
  print("Most common element : $maxElement with $max apparition");
  print("Result : $max - $min = ${max - min}");
}
