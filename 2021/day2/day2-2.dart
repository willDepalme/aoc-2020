import 'dart:io';

main(List<String> args) {
  File input = new File("input");
  
  int posX = 0;
  int posY = 0;
  int aim = 0;

  Map<String, Function(int)> commands = {
    "forward": (pos) { posX+=pos; posY+=(aim*pos);},
    "up": (pos) { aim-=pos; },
    "down": (pos) { aim+=pos; },
  };

  input.readAsLines().then((lines) {
    lines.forEach((line) async {
      print("Position (X: $posX, Y: $posY)");
      String command = line.split(" ")[0];
      int move = int.parse(line.split(" ")[1]);
      commands[command]?.call(move);
    });

    print(posX * posY);
  });
}
