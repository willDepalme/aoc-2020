import 'dart:io';

main(List<String> args) {
  File input = new File("input");
  int? prevDeepLevel;
  int count = 0;
  input.readAsLines().then((lines) {
    for (int i = 2; i < lines.length; i++) {
      int deepLevel = int.parse(lines[i-2]) + int.parse(lines[i-1]) + int.parse(lines[i]);
      if (prevDeepLevel != null && prevDeepLevel! < deepLevel) {
          count++;
      }
      prevDeepLevel = deepLevel;
    }

    print(count);
  });
}
