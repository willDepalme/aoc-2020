import 'dart:io';

main(List<String> args) {
  File input = new File("input");

  Map<int, Map<String, int>> data = {};

  List<String> lines = input.readAsLinesSync();

  List<String> possibleOxygenRatings = [];
  possibleOxygenRatings.addAll(lines);
  String prefixOxygen = "";

  while (possibleOxygenRatings.length > 1) {
    print("Prefix : $prefixOxygen  => ${possibleOxygenRatings.length}");
    prefixOxygen += possibleOxygenRatings
                .where((el) => el.startsWith(prefixOxygen + "0"))
                .length >
            possibleOxygenRatings.length / 2
        ? "0"
        : "1";

    if (possibleOxygenRatings
            .where((element) => element.startsWith(prefixOxygen))
            .length ==
        0) {
      break;
    }
    possibleOxygenRatings = possibleOxygenRatings
        .where((element) => element.startsWith(prefixOxygen))
        .toList();
  }
  print(possibleOxygenRatings);
  print("OxygenRating : ${possibleOxygenRatings.elementAt(0)}");
  print("=> ${int.parse(possibleOxygenRatings.elementAt(0), radix: 2)}");

  List<String> possibleCO2Ratings = [];
  possibleCO2Ratings.addAll(lines);
  String prefixCO2 = "";

  while (possibleCO2Ratings.length > 1) {
    print("Prefix : $prefixCO2  => ${possibleCO2Ratings.length}");
    prefixCO2 += possibleCO2Ratings
                .where((el) => el.startsWith(prefixCO2 + "0"))
                .length <=
            possibleCO2Ratings.length / 2
        ? "0"
        : "1";
    if (possibleCO2Ratings
            .where((element) => element.startsWith(prefixCO2))
            .length ==
        0) {
      break;
    }
    possibleCO2Ratings = possibleCO2Ratings
        .where((element) => element.startsWith(prefixCO2))
        .toList();
  }
  print("CO2Rating : ${possibleCO2Ratings.elementAt(0)}");
  print("=> ${int.parse(possibleCO2Ratings.elementAt(0), radix: 2)}");

  print(
      "Result = ${int.parse(possibleOxygenRatings.elementAt(0), radix: 2) * int.parse(possibleCO2Ratings.elementAt(0), radix: 2)}");
}
