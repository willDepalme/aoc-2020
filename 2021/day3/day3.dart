import 'dart:io';

main(List<String> args) {
  File input = new File("input");

  Map<int, Map<String, int>> data = {};

  List<String> lines = input.readAsLinesSync();
  lines.forEach((line) {
    for (int i = 0; i < line.length; i++) {
      if (!data.containsKey(i)) {
        data[i] = {"0": 0, "1": 0};
      }

      data[i]![line[i]] = data[i]![line[i]]! + 1;
    }
  });
  print(data);

  String gamma = "";
  String epsilon = "";

  data.forEach((position, value) {
    gamma += value["0"]! > value["1"]! ? "0" : "1";
    epsilon += value["0"]! < value["1"]! ? "0" : "1";
  });

  int gammaInt = int.parse(gamma, radix: 2);
  int epsilonInt = int.parse(epsilon, radix: 2);

  print("Gamma $gamma => $gammaInt");
  print("Epsilon $epsilon => $epsilonInt");

  print("Result : ${gammaInt * epsilonInt}");
}
