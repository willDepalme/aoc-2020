import 'dart:io';

main(List<String> args) {
  File input = new File("input");

  List<BingoGrid> grids = [];

  /** Parse data grids */
  List<String> lines = input.readAsLinesSync();
  List<int> drawNumbers =
      lines.first.split(',').map((e) => int.parse(e)).toList();

  List<String> gridLines = [];
  lines.skip(2).forEach((line) {
    if (line.length == 0 && gridLines.isNotEmpty) {
      BingoGrid grid = BingoGrid.fromLines(gridLines);
      // print(grid);
      grids.add(grid);
      gridLines = [];
    } else {
      gridLines.add(line);
    }
  });

  // Tirage
  List<BingoGrid> alreadyWinGrids = [];
  BingoGrid? lastWinningGrid = null;
  int? winningDrawNumber = null;
  for (int number in drawNumbers) {
    print("Draw number $number");

    List<BingoGrid> filteredGrids = grids.where((g) => !g.ignore).toList();

    for (BingoGrid grid in filteredGrids) {
      grid.containsAndCheckNumber(number);
    }

    for (BingoGrid grid in filteredGrids) {
      //print(grid.displayChecked());
      if (grid.isWin()) {
        if (!alreadyWinGrids.any((g) => g.getId() == grid.getId())) {
          winningDrawNumber = number;
          print("WINNER !!!");
          print(grid.displayChecked());
          grid.ignore = true;
          lastWinningGrid = grid;
          alreadyWinGrids.add(grid);
        }
      }
    }
  }

  print("Last winning grid");
  print(lastWinningGrid?.displayChecked());
  print("Sum = ${lastWinningGrid?.calcSumBoard()}");
  print("Winning Number $winningDrawNumber");
  print("Result = ${winningDrawNumber! * lastWinningGrid!.calcSumBoard()}");
}

class BingoGrid {
  static const int gridLength = 5;

  final List<List<int>> numbers = [];

  final List<List<int?>> checkedNumbers =
      List.filled(gridLength, List.filled(gridLength, null));

  bool ignore = false;

  BingoGrid();

  factory BingoGrid.fromLines(List<String> lines) {
    BingoGrid grid = BingoGrid();

    lines.forEach((line) {
      try {
        grid.numbers.add(line
            .split(" ")
            .where((e) => e != " " && e != "")
            .map((e) => int.parse(e))
            .toList());
      } catch (err) {
        print("Error in line $line");
      }
    });

    return grid;
  }

  bool containsAndCheckNumber(int number) {
    for (int i = 0; i < gridLength; i++) {
      List<int> line = numbers[i];
      if (line.contains(number)) {
        List<int?> lineToUpdate = []..addAll(checkedNumbers[i]);
        lineToUpdate[line.indexOf(number)] = number;
        checkedNumbers[i] = lineToUpdate;
        return true;
      }
    }
    return false;
  }

  bool isWin() {
    // check rows
    for (List<int?> row in checkedNumbers) {
      if (!row.contains(null)) {
        return true;
      }
    }

    // check columns
    for (int col = 0; col < gridLength; col++) {
      bool ok = true;
      for (int row = 0; row < gridLength; row++) {
        if (checkedNumbers[row][col] == null) {
          ok = false;
        }
      }
      if (ok) {
        return true;
      }
    }

    return false;
  }

  int calcSumBoard() {
    int sum = 0;
    for (int i = 0; i < gridLength; i++) {
      for (int j = 0; j < gridLength; j++) {
        if (checkedNumbers[i][j] == null) {
          //print("Add ${numbers[i][j]}");
          sum += numbers[i][j];
        }
      }
    }
    print("Sum of grid unchecked is $sum");
    return sum;
  }

  String toString() {
    String display = "";
    numbers.forEach((line) {
      display += line
              .map((e) => e.toString().padLeft(2, " "))
              .join(" ")
              .padLeft(2, " ") +
          "\n";
    });
    return display;
  }

  String displayChecked() {
    String display = "";
    checkedNumbers.forEach((line) {
      display += line
              .map((e) => e != null ? e.toString().padLeft(2, " ") : " x")
              .join(" ") +
          "\n";
    });
    return display;
  }

  String getId() {
    String display = "";
    numbers.forEach((line) {
      display += line.map((e) => e.toString().padLeft(2, " ")).join("-") + "-";
    });
    return display;
  }
}
