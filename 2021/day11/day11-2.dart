import 'dart:io';

import 'dart:math';

int nbFlash = 0;
const int size = 10;
const int nbStep = 100;

main(List<String> args) {
  File input = new File("input");
  List<String> lines = input.readAsLinesSync();

  List<List<int>> octopus = [];

  lines.forEach((line) {
    octopus.add(line.split("").map((e) => int.parse(e)).toList());
  });

  printOctopus(octopus);
  bool allFlashed = false;
  int step = 0;
  while (!allFlashed) {
    step++;
    incrementMap(octopus);
    checkFlashes(octopus);
    allFlashed = cleanFlashed(octopus);
    print("\nAfter step $step");
    printOctopus(octopus);
  }

  print("All flashed at step $step");
}

void incrementMap(List<List<int>> map) {
  for (int y = 0; y < size; y++) {
    for (int x = 0; x < size; x++) {
      map[y][x] = map[y][x] + 1;
    }
  }
}

void checkFlashes(List<List<int>> map) {
  List<Point> flashed = [];

  for (int y = 0; y < size; y++) {
    for (int x = 0; x < size; x++) {
      if (map[y][x] > 9) {
        flashOctopus(map, Point(x, y), flashed, 0);
      }
    }
  }
}

void flashOctopus(
    List<List<int>> map, Point<int> start, List<Point> flashed, int level) {
  map[start.y][start.x] = map[start.y][start.x] + 1;
  if (map[start.y][start.x] > 9 && !flashed.contains(start)) {
    //print(("  " * level) + "Flash ${start.x},${start.y}");
    nbFlash++;
    flashed.add(start);

    for (int y = start.y - 1; y <= start.y + 1; y++) {
      for (int x = start.x - 1; x <= start.x + 1; x++) {
        if (y >= 0 &&
            x >= 0 &&
            y < size &&
            x < size &&
            (y != start.y || x != start.x)) {
          flashOctopus(map, Point(x, y), flashed, level + 1);
        }
      }
    }
  }
}

bool cleanFlashed(List<List<int>> map) {
  int sum = 0;
  for (int y = 0; y < size; y++) {
    for (int x = 0; x < size; x++) {
      if (map[y][x] > 9) {
        map[y][x] = 0;
      }
      sum += map[y][x];
    }
  }

  return sum == 0;
}

void printOctopus(List<List<int>> map) {
  for (int y = 0; y < size; y++) {
    print(map[y].join(" "));
  }
  print("\nNb flash : $nbFlash");
}
