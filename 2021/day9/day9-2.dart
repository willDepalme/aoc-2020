import 'dart:io';

import 'dart:math';

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  List<List<int>> floor = [];

  lines.forEach((line) {
    floor.add(line.split("").map((e) => int.parse(e)).toList());
  });

  List<int> areaTotal = [];

  for (int y = 0; y < floor.length; y++) {
    for (int x = 0; x < floor[y].length; x++) {
      int value = floor[y][x];
      bool lowest = true;

      // Check up
      if (y - 1 >= 0 && floor[y - 1][x] <= value) {
        lowest = false;
      }

      // Check Down
      if (y + 1 < floor.length && floor[y + 1][x] <= value) {
        lowest = false;
      }

      // Check Left
      if (x - 1 >= 0 && floor[y][x - 1] <= value) {
        lowest = false;
      }

      // Check Right
      if (x + 1 < floor[y].length && floor[y][x + 1] <= value) {
        lowest = false;
      }

      if (lowest) {
        print("Lowest point found at ($x, $y) with value $value");
        print("Calculating area...");
        areaTotal.add(getBottomArea(x, y, floor));
      }
    }
  }

  print("\nSorting areas sizes...\n");
  areaTotal.sort();
  int total = 1;
  for (int i = areaTotal.length - 1; i > areaTotal.length - 4; i--) {
    total *= areaTotal[i];
    print("Multiplying ${areaTotal[i]}");
  }
  print("Total area surface : $total");
}

int getBottomArea(int x, int y, List<List<int>> floor) {
  /**      
   *         
   *       ^  y-1
   *       |
   *  x-1  |  
   * <--- x,y --->
   *       |  x+1
   *       |
   *  y+1  v
   * 
   */

  Set<Point<int>> basinArea = Set();

  discoverBasinArea(Point(x, y), floor, basinArea);

  print("Area found of ${basinArea.length}");
  return basinArea.length;
}

void discoverBasinArea(
    Point<int> start, List<List<int>> floor, Set<Point> basinArea) {
  if (floor[start.y][start.x] == 9 || basinArea.contains(start)) {
    return;
  } else {
    basinArea.add(start);

    if (start.y - 1 >= 0) {
      discoverBasinArea(Point(start.x, start.y - 1), floor, basinArea);
    }
    if (start.y + 1 < floor.length) {
      discoverBasinArea(Point(start.x, start.y + 1), floor, basinArea);
    }
    if (start.x - 1 >= 0) {
      discoverBasinArea(Point(start.x - 1, start.y), floor, basinArea);
    }
    if (start.x + 1 < floor[start.y].length) {
      discoverBasinArea(Point(start.x + 1, start.y), floor, basinArea);
    }
  }
}
