import 'dart:io';

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  List<List<int>> floor = [];

  lines.forEach((line) {
    floor.add(line.split("").map((e) => int.parse(e)).toList());
  });

  int sum = 0;
  for (int y = 0; y < floor.length; y++) {
    for (int x = 0; x < floor[y].length; x++) {
      int value = floor[y][x];
      bool lowest = true;

      // Check up
      if (y - 1 >= 0 && floor[y - 1][x] <= value) {
        lowest = false;
      }

      // Check Down
      if (y + 1 < floor.length && floor[y + 1][x] <= value) {
        lowest = false;
      }

      // Check Left
      if (x - 1 >= 0 && floor[y][x - 1] <= value) {
        lowest = false;
      }

      // Check Right
      if (x + 1 < floor[y].length && floor[y][x + 1] <= value) {
        lowest = false;
      }

      if (lowest) {
        print("Lowest point found at ($x, $y) with value $value");
        sum += value + 1;
      }
    }
  }

  print("Total = $sum");
}
