import 'dart:io';

import 'dart:math';

main() {
  File input = File("input");

  List<String> lines = input.readAsLinesSync();

  List<Dot> points = [];
  List<Instruction> folds = [];
  int maxX = 0;
  int maxY = 0;

  lines.forEach((line) {
    if (line.contains(",")) {
      int x = int.parse(line.split(",")[0]);
      int y = int.parse(line.split(",")[1]);
      maxX = max(maxX, x);
      maxY = max(maxY, y);
      points.add(Dot(x, y));
    } else if (line.isNotEmpty) {
      line = line.replaceAll("fold along ", "");
      folds.add(Instruction(int.parse(line.split("=")[1]), line.split("=")[0]));
    }
  });

  print("Initial grid $maxX x $maxY");

  folds.forEach((fold) {
    applyInstruction(fold, points);
  });

  Set<Dot> distinct = Set();
  maxX = 0;
  maxY = 0;
  points.forEach((p) {
    if (!distinct.any((element) => element.x == p.x && element.y == p.y)) {
      distinct.add(p);
      maxX = max(maxX, p.x);
      maxY = max(maxY, p.y);
    }
  });
  print("Folded grid $maxX x $maxY");
  printGrid(distinct, maxX, maxY);

  print("${points.length} points");
  print("${distinct.length} distinct points");
}

void applyInstruction(Instruction inst, List<Dot> points) {
  if (inst.axe == "x") {
    points.where((p) => p.x > inst.pos).forEach((point) {
      int delta = point.x - inst.pos;
      point.x = inst.pos - delta;
    });
  } else {
    points.where((p) => p.y > inst.pos).forEach((point) {
      int delta = point.y - inst.pos;
      point.y = inst.pos - delta;
    });
  }
}

void printGrid(Set<Dot> points, int maxX, int maxY) {
  for (int y = 0; y <= maxY; y++) {
    String line = "";
    for (int x = 0; x <= maxX; x++) {
      line += points.any((e) => e.y == y && e.x == x) ? "#" : ".";
    }
    print(line);
  }
}

class Dot {
  int x;
  int y;

  Dot(this.x, this.y);

  @override
  bool equals(Object? o1, Object? o2) =>
      (o1 as Dot).x == (o2 as Dot).x && (o1 as Dot).x == (o2 as Dot).y;
}

class Instruction {
  int pos;
  String axe;

  Instruction(this.pos, this.axe);
}
