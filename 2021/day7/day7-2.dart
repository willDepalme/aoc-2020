import 'dart:io';

main(List<String> args) {
  File input = new File("input");

  List<String> lines = input.readAsLinesSync();

  List<int> crabsPos = lines[0].split(",").map((e) => int.parse(e)).toList();
  crabsPos.sort();

  Map<int, int> countByPos = {};
  crabsPos.forEach((pos) {
    if (!countByPos.containsKey(pos)) {
      countByPos[pos] = crabsPos.where((e) => e == pos).length;
    }
  });

  int minPos = crabsPos.first;
  int maxPos = crabsPos.last;

  int minTotalCount = 0;
  int minCostPos = 0;

  print("Check from pos $minPos to $maxPos");
  for (int posToGo = minPos; posToGo <= maxPos; posToGo++) {
    int totalCount = 0;
    countByPos.forEach((pos, count) {
      if (pos != posToGo) {
        int steps = (posToGo - pos).abs();
        totalCount += count * getCostFromSteps(steps);
      }
    });

    print("Cost for $posToGo => $totalCount");
    if (minTotalCount == 0 || totalCount < minTotalCount) {
      minTotalCount = totalCount;
      minCostPos = posToGo;
    }
  }
  print("Pos optimized : $minCostPos with cost of $minTotalCount");
}

int getCostFromSteps(int steps) {
  int cost = 0;
  for (int i = 0; i < steps; i++) {
    cost += i + 1;
  }
  return cost;
}
