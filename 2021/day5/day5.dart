import 'dart:io';

import 'dart:math';

main(List<String> args) {
  File input = new File("input");
  const int mapSize = 1000;

  List<String> lines = input.readAsLinesSync();

  // Fill map with 0
  final List<List<int>> map =
      List.generate(mapSize, (i) => List.generate(mapSize, (i2) => 0));

  lines.forEach((line) {
    parseLine(line, map);
  });

  int count = 0;
  map.forEach((row) {
    //print(row.map((e) => e > 0 ? e.toString() : ".").join(" "));
    count += row.where((element) => element >= 2).length;
  });
  print("Overlaping points : $count");
}

void parseLine(String line, final List<List<int>> map) {
  print(line);
  String startPos = line.split(" -> ")[0];
  String endPos = line.split(" -> ")[1];

  int x1 = int.parse(startPos.split(',')[0]);
  int y1 = int.parse(startPos.split(',')[1]);

  int x2 = int.parse(endPos.split(',')[0]);
  int y2 = int.parse(endPos.split(',')[1]);

  bool horizontal = y1 == y2;
  bool vertical = x1 == x2;
  print("H $horizontal - V $vertical");

  if (horizontal || vertical) {
    print((horizontal ? "Horizontal" : "Vertical") + " line");
    int step = max((x1 - x2).abs(), (y1 - y2).abs());

    if (horizontal) {
      int dir = x1 < x2 ? 1 : -1;
      for (int i = 0; i <= step; i++) {
        int x = x1 + (dir * i);
        map[y1][x] = map[y1][x] + 1;
      }
    } else {
      int dir = y1 < y2 ? 1 : -1;
      for (int i = 0; i <= step; i++) {
        int y = y1 + (dir * i);
        map[y][x1] = map[y][x1] + 1;
      }
    }
  } else {
    print("Ignored");
  }
}
