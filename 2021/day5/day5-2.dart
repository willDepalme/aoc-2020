import 'dart:io';

import 'dart:math';

main(List<String> args) {
  File input = new File("input");
  const int mapSize = 1000;

  List<String> lines = input.readAsLinesSync();

  // Fill map with 0
  final List<List<int>> map =
      List.generate(mapSize, (i) => List.generate(mapSize, (i2) => 0));

  lines.forEach((line) {
    parseLine(line, map);
  });

  int count = 0;
  map.forEach((row) {
    //print(row.map((e) => e > 0 ? e.toString() : ".").join(" "));
    count += row.where((element) => element >= 2).length;
  });
  print("Overlaping points : $count");
}

void parseLine(String line, final List<List<int>> map) {
  //print(line);
  String startPos = line.split(" -> ")[0];
  String endPos = line.split(" -> ")[1];

  int x1 = int.parse(startPos.split(',')[0]);
  int y1 = int.parse(startPos.split(',')[1]);

  int x2 = int.parse(endPos.split(',')[0]);
  int y2 = int.parse(endPos.split(',')[1]);

  int step = max((x1 - x2).abs(), (y1 - y2).abs());

  int horDir = x2 - x1 > 0 ? 1 : -1;
  int verDir = y2 - y1 > 0 ? 1 : -1;

  for (int i = 0; i <= step; i++) {
    int x = (x1 - x2 == 0) ? x1 : x1 + (horDir * i);
    int y = (y1 - y2 == 0) ? y1 : y1 + (verDir * i);
    //print("Increment $x,$y");
    map[y][x] = map[y][x] + 1;
  }
}
